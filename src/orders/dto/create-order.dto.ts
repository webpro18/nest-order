class CreatedOrderItemDto {
  productId: number;
  amount: number;
  total: number;
}
export class CreateOrderDto {
  customerId: number;
  orderItems: CreatedOrderItemDto[];
}
